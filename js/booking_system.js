var ensurePopupsCreated = function() {
	// We need to ensure there is a popups-container available to house our popups, lightboxes etc
	// We mainly need this so we can scope the CSS so it doesn't interfere as with booking-container
	popupsContainer = $('.booking-popups');
	if (popupsContainer.length === 0) {
		popupsContainer = $('<div class="booking-popups"></div>');
		$('body').prepend(popupsContainer);
	}
};

/*
 * jQuery Tiny Pub/Sub
 * https://github.com/cowboy/jquery-tiny-pubsub
 * USED TO PUBLISH TAB CHANGES
 * Copyright (c) 2013 "Cowboy" Ben Alman
 * Licensed under the MIT license.
 */

(function($) {

  var o = $({});

  $.subscribe = function() {
    o.on.apply(o, arguments);
  };

  $.unsubscribe = function() {
    o.off.apply(o, arguments);
  };

  $.publish = function() {
    o.trigger.apply(o, arguments);
  };

}(jQuery));

// 1.0.0 - CRUISE RESULTS
$(function(){
	// BEGIN DATE SELECTOR
	$('.more-dates-panel-close').click(function(event){
		event.stopPropagation();
		event.preventDefault();
		$('.more-dates-panel').slideUp();
	});
	$('.cruise-more-dates').click(function(event){
		event.stopPropagation();
		event.preventDefault();
		var target = $(event.currentTarget);
		var moreDatePopup = $('#' + target.data('popupdates'));
		moreDatePopup.slideDown();
	});
	$('.date-box').click(function(event){
		event.stopPropagation();
		event.preventDefault();

		// CODEGEN ACTION REQUIRED: CALL FUNCTION TO UPDATE DATES

		$('.more-dates-panel').fadeOut();
	});

	// HOVERS AND SELECTS THAT CHANGE PRICE DISPLAYED
	$('.cruise-date-select-button').click(function(event){
		event.stopPropagation();
		event.preventDefault();
		var target = $(event.currentTarget);
		var lastSelected = target.siblings('.cruise-date-selected');
		lastSelected.addClass('cruise-date-unselected').removeClass('cruise-date-selected');
		target.addClass('cruise-date-selected').removeClass('cruise-date-unselected');
		var priceParent = target.parent().parent();
		var showPrice = $('.' + target.data('pricing'));
		priceParent.find('.price-date-wrapper').hide();
		priceParent.find(showPrice).show();
	});

	$('.cruise-date-select-button').mouseenter(function(event){
		event.stopPropagation();
		event.preventDefault();
		var target = $(event.currentTarget);
		var priceParent = target.parent().parent().parent();
		var showPrice = $('.' + target.data('pricing'));
		priceParent.find('.price-date-wrapper').hide();
		priceParent.find(showPrice).show();
	});

	$('.cruise-date-select-button').mouseleave(function(event){
		event.stopPropagation();
		event.preventDefault();
		var target = $(event.currentTarget);
		var priceParent = target.parent().parent().parent();
		var whichPrice = priceParent.find('.cruise-date-selected');
		var showPrice = $('.' + whichPrice.data('pricing'));
		priceParent.find('.price-date-wrapper').hide();
		priceParent.find(showPrice).show();
	});


	// END DATE SELECTOR

	//$.subscribe('booking:tabChange', function() {
	//	console.log("changed");
	//});

	// BEGIN TAB SECTION EXPANDER
	var selectTab = function(expandableElementId, tabName, toggle) {
		// get the data attribute value for target and tab
		var expandableElement = $('#' + expandableElementId);
		var selectedTab = $('.' + tabName);

		// we need to give the middle tab special classes to achieve the dropshadow effect
		var middleTab = expandableElement.find('.tab-2');
		if (tabName == "tab-3"){
			middleTab.removeClass('tab-shadow-right').addClass('tab-shadow-left');
		} else if (tabName == "tab-1"){
			middleTab.removeClass('tab-shadow-left').addClass('tab-shadow-right');
		}

		// publish this event so that BB code can hook in to it
		$.publish('booking:tabChange');

		if (expandableElement.find(selectedTab).data('expanded') && toggle) {
			// tab is already selected and they clicked a link which should toggle the tabs (close if already open)
			// open the expandable
			expandableElement.slideUp('slow', function() {
				expandableElement.find('.tabbed-tabs li.tab-selected').removeClass('tab-selected').data('expanded', false);
			});

			// switch the link state
			$('.tab-expander[data-tab=' + tabName + '][data-target=' + expandableElementId + ']').removeClass('tab-expander-active');
		} else {
			// deselect the tabs
			expandableElement.find('.tabbed-tabs li.tab-selected').removeClass('tab-selected').data('expanded', false);
		
			// hide all the tab content
			expandableElement.find('.tab-content').hide();

			// select the right tab
			expandableElement.find(selectedTab).addClass('tab-selected').data('expanded', true);

			// show the right content
			expandableElement.find('.tabbed-content').find(selectedTab).show();

			// open the expandable
			expandableElement.slideDown('slow');
		
			// switch the link state
			$('.tab-expander[data-target=' + expandableElementId + ']').removeClass('tab-expander-active');
			$('.tab-expander[data-tab=' + tabName + '][data-target=' + expandableElementId + ']').addClass('tab-expander-active');
		}
	};

	// Handle non-tab links
	$('.tab-expander').click(function(event){
		event.stopPropagation();
		event.preventDefault();
		var target = $(event.currentTarget);
		selectTab(target.data('target'), target.data('tab'), true);
	});
	// Also need to handle tab clicks
	$('.tabbed-tabs li').click(function(event){
		var target = $(event.currentTarget);
		selectTab(target.parents('.expandable').attr('id'), target.data('tab'));

	});
	// Also need to handle a button close
	$('.tabbed-close').click(function(event){
		var target = $(event.currentTarget);
		target.parent().parent().parent().slideUp('slow');
	});
	// END TABBED SECTION EXPANDER	

	// BEGIN HACK TO HIDE NEW TO CRUISING POPUP
	$.subscribe('expander:expand', function(event, id) {
		if (id == 'results-more-options') {
			$('.booking-popups #popup-stateroom').addClass('hide');
		}
	});
	// END HACK TO HIDE NEW TO CRUISING POPUP
});

// 2.0.1a - STATE ROOM SELECT STEP 1
// STATEROOM SELECTION STEP 1 JS
$(function() {
	// BEGIN EXPLANATION SHOW
	$('.show-explanation-on-check').change(function(event) {
		var target = $(event.currentTarget);
		var toShow = $('#' + target.data('explanation'));
		if (target.attr('checked')) {
			toShow.fadeIn();
		} else {
			toShow.fadeOut();
		}
	});
	// END EXPLANATION SHOW
});

// 2.0.1b - STATE ROOM SELECT STEP 2
$(function(){
	// BEGIN ROOM TYPE SELECTOR AND IMAGE SELECTOR
	$('.room-type-images-main-image .image1').show();
	$('.room-type-inside').fadeIn();
	$('.room-type-selector li').click(function(event){
		event.stopPropagation();
		event.preventDefault();
		var target = $(event.currentTarget);
		// get the data attribute value for target and tab
		var roomTypetoShow = $('.room-type-' + target.data('room'));
		// strip off the selection from all
		$('.room-type-selector li').removeClass('selected');
		// and the selection
		target.addClass('selected');
		// hide all the rooms
		$('.room-type-details').hide();
		// show the correct room
		roomTypetoShow.fadeIn();
	});
	$('.room-type-image-selector li').click(function(event){
		event.stopPropagation();
		event.preventDefault();
		var target = $(event.currentTarget);
		var roomType = $('.room-type-' + target.data('room'));
		var container = target.parents('.room-type-images');
		var showImage = container.find('.' + target.data('image'));
		container.find('.room-type-images-main-image img').hide();
		showImage.fadeIn();
	});
	// END ROOM TYPE SELECTOR AND IMAGE SELECTOR	
});

// 2.0.1c - STATE ROOM SELECT STEP
// STATEROOM SELECTION STEP 3 JS
$(function() {
	// BEGIN SHOW / HIDE STATEROOM SECTIONS
	$('.stateroom-section').on('click', '.icon-plus-sign-alt', function(event) {
		event.stopPropagation();
		event.preventDefault();
		$('.stateroom-section-expanded .icon-minus-sign-alt').click();
		var target = $(event.currentTarget);
		var container = target.parents('.stateroom-section').find('.stateroom-section-content');
		var currentHeight = container.height();
		var autoHeight = container.css('height', 'auto').height();
		container.height(currentHeight).animate({ height: autoHeight }, 600, function() {
			container.css('height', 'auto').parents('.stateroom-section').addClass('stateroom-section-expanded');
		});
	});
	$('.stateroom-section').on('click', '.icon-minus-sign-alt', function(event) {
		event.stopPropagation();
		event.preventDefault();
		var target = $(event.currentTarget);
		var container = target.parents('.stateroom-section').find('.stateroom-section-content');
		$('.stateroom-explanation:not(.explaination-stateroom-options)').fadeOut();
		container.animate({height: 1}, function() {
			container.parents('.stateroom-section').removeClass('stateroom-section-expanded');
		});
	});
	// END SHOW / HIDE STATEROOM SECTIONS

	// BEGIN OFFER INFO
	$('.show-offer-info').click(function(event) {
		event.preventDefault();
		var target = $(event.currentTarget);
		var targetOfferExplanation = $('#stateroom-offer-' + target.data('offer'));
		$('.col-stateroom-explanations .stateroom-explanation:not(.hide)').fadeOut(function() {
			$(this).addClass('hide');
			targetOfferExplanation.show().removeClass('hide');
			var arrow = targetOfferExplanation.find('.icon-caret-left');
			var offset = ($('.col-stateroom-explanations').offset().top - target.offset().top + arrow.position().top) * -1;
			targetOfferExplanation.css({top: offset});
		});
	});
	// END OFFER INFO

	// BEGIN SHOW AVAILABLE STATEROOMS
	$('.show-available-staterooms').click(function(event) {
		event.preventDefault();
		var target = $(event.currentTarget);
		var container = target.parents('.stateroom-selection');
		var loadingLightbox = $('#lightbox-loading-staterooms').bookingLightbox({
			fixed: true
		});

		// CODEGEN ACTION REQUIRED: BEGIN LOADING OF STATEROOMS HERE
		window.setTimeout(function() {
			// ON RESPONSE HIDE THE BUTTON THAT THE USER CLICKED
			container.find('.staterooms-pre-loaded').hide();

			// INSERT NEW CONTENT INTO DOM HERE AND SHOW
			container.find('.staterooms-post-loaded').show();

			loadingLightbox.trigger('close');

			// SET UP EVENT TO HIGHLIGHT ROW WHEN THE USER SELECTS THE RADIO
			container.find('input[type=radio]').change(function(event) {
				container.find('.selected').removeClass('selected');
				$(this).parents('.row').addClass('selected');
			});

		}, 2000);
	});
	// END SHOW AVAILABLE STATEROOMS

	// BEGIN UPSELL LIGHTBOX ON CONFIRM
	$('.confirm-stateroom-selection').click(function(event) {
		event.preventDefault();
		$('#lightbox-upsell').bookingLightbox({
			fixed: true
		});
	});
	// END UPSELL LIGHTBOX ON CONFIRM

	// BEGIN STATEROOM OFFER SELECTION
	$('.row-offer-selection').find('input[type=radio]').change(function(event) {
		var target = $(event.currentTarget);
		var container = target.parents('.offer-row');
		container.siblings().removeClass('offer-row-selected');
		container.addClass('offer-row-selected');
	});
	// END STATEROOM OFFER SELECTION
});

// 3.0 - HOTEL LISTINGS
$(function() {
	// BEGIN ALL IMAGE LIGHTBOX
	$('.hotel-info-show-all-images').click(function(event) {
		event.stopPropagation();
		event.preventDefault();

		var target = $(event.currentTarget);
		var imageSetId = target.data('hotelImageSet');
		var imageSet = window.hotelImageSets[imageSetId];

		// make our lightbox element
		var element = $('#lightbox-template').bookingLightbox({
			fixed: true,
			noShow: true
		});
		element.data({ currentIndex: 0, imageSet: imageSet });

		// create a switchToIndex event which will change the current image
		element.bind('switchToIndex', function(event, index) {
			// use this animating variable to prevent starting another animation before
			// the last one has finished
			if (element.data('animating')) return;
			element.data('animating', true);

			element.data('currentIndex', index);
			var imageToShow = element.data('imageSet')[index];
			var newImageElement = $('<img>');
			newImageElement.load(function() {
				var thisImage = this;
				element.find('.lightbox-main-image-container img').fadeOut(function() {
					var oldImage = this;
					// there are some quite hacky hardcoded values here
					element.find('.lightbox-main-container').animate({ width: (thisImage.width + 25) + 'px', height: (thisImage.height + 50) + 'px'}, function() {
						thisImage = $(thisImage).hide();
						$(oldImage).replaceWith(thisImage);
						thisImage.fadeIn(function() {
							// make sure to set this so that we are able to start a new animation later
							element.data('animating', false);
						});
					}).css('overflow', 'visible');
				});
			}).attr('src', imageToShow);
			
			// set the current index and total image values in the lightbox footer
			element.find('.lightbox-current-image-index').text(index + 1);
			element.find('.lightbox-image-total').text(element.data('imageSet').length);
		});

		// set up the scroll left listener
		element.find('.lightbox-scroll-left').click(function(event) {
			if (element.data('currentIndex') === 0) {
				element.trigger('switchToIndex', [element.data('imageSet').length - 1]);
			} else {
				element.trigger('switchToIndex', [element.data('currentIndex') - 1]);
			}
		});

		// set up the scroll right listener
		element.find('.lightbox-scroll-right').click(function(event) {
			if (element.data('currentIndex') == (element.data('imageSet').length - 1)) {
				element.trigger('switchToIndex', [0]);
			} else {
				element.trigger('switchToIndex', [element.data('currentIndex') + 1]);
			}
		});

		// now that everything is set up, load the first image and fade in the lightbox
		element.trigger('switchToIndex', [0]);
		element.fadeIn();
	});
	// pre load all the lightbox images
	for (var i in window.hotelImageSets) {
		var imageSet = window.hotelImageSets[i];
		$(imageSet).each(function(){
			$('<img/>')[0].src = this;
        });
	}
	// END ALL IMAGE LIGHTBOX

	// BEGIN ROOM TYPE SELECTION
	$('.hotel-info-room-types').find('input[type=radio]').change(function(event) {
		var target = $(event.currentTarget);
		$('.hotel-info-room-types tr').removeClass('active');
		target.parents('tr').addClass('active');

		var resultContainer = target.parents('.hotel-result-container');
		var pricingContainer = resultContainer.find('.col-hotel-result-price');
		pricingContainer.find('.overall-price').text(target.data('overall-price'));
		pricingContainer.find('.nightly-price').text(target.data('nightly-price'));
		pricingContainer.find('.room-type').text(target.data('room-type'));
	});
	// END ROOM TYPE SELECTION

	// BEGIN SHOW MAP
	$('.show-map').click(function(event) {
		event.stopPropagation();
		event.preventDefault();

		var target = $(event.currentTarget);

		// make map lightbox
		var element = $('#lightbox-basic-template').bookingLightbox({
			fixed: true
		});

		// PUT GOOGLE MAP INITIALISION JS HERE
		var mapOptions = {
			zoom: 8,
			center: new google.maps.LatLng(-34.397, 150.644),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var map = new google.maps.Map(element.find('.lightbox-main-map-container')[0], mapOptions);
	});
	// END SHOW MAP
});

// 4.0 - FLIGHT LISTINGS

$(function(){
	// BEGIN AIRPORT AUTOCOMPLETE

	// CODEGEN ACTION REQUIRED: test data, can be removed in production
	var airports = [
		{ value: 'Miami International', data: 'MIA' },
		{ value: 'London Heathrow (LHR)', data: 'LHR' }
	];

	// AUTOCOMPLETE - https://github.com/devbridge/jQuery-Autocomplete
	$('#outbound-from-city').autocomplete({
		// use a serviceURL instead of lookup in production
		//serviceUrl: '/autocomplete/countries',
		lookup: airports,
		onSelect: function(suggestion){
			// function when clicked goes here
		}
	});
	// END AIRPORT AUTOCOMPLETE
});


// 5.0 - SUMMARY
$(function() {
	$('.link-return-to-search').click(function(event) {
		event.stopPropagation();
		event.preventDefault();

		var target = $(event.currentTarget);

		// create container element for our lightbox which we will hang events off
		var element = $('#lightbox-warning').bookingLightbox({
			fixed: true
		});
	});


});

// 6.0 - BOOKING ABOUT YOU
$(function(){

	// BEGIN MIDDLE NAMES RADIO LOGIC
	// Disables and enables the associated input field
	$('.guest-details-middle-names').find('input[type=radio]').change(function(event) {
		var input = $(event.currentTarget);
		var middleNameInput = input.parent().parent().find('input[type=text]');
		var activeInput = input.attr('class');
		if (activeInput == "middle-name-activated") {
			middleNameInput.prop('disabled', false).removeClass('slvzr-disabled');
		} else {
			middleNameInput.prop('disabled', true).val('').addClass('slvzr-disabled');
		}
	});

	// BEGIN DATE PICKER
	// NOTE: Need to update Calendar icon in the datepicker css/less file
	// http://jqueryui.com/datepicker/
	$('.booking-container .datepicker').datepicker({
      changeMonth: true,
      changeYear: true,
      dateFormat: "dd/mm/yy",
      showOn: "button",
      // this is needed but overwritten by CSS skin image
      buttonImage: "../images/ibe_generic/icon_calendar_default.png",
      buttonImageOnly: true,
      yearRange: "1900:2020"
    });
    // END DATEPICKER
});


// 9.0 - BOOKING PAYMENT
$(function() {
	// BEGIN PAYMENT RADIO LOGIC
	$('.payment-details-today').find('input[type=radio]').change(function(event) {
		// When the radio buttons for paying just the deposit today, or the full amount change
		// We need to update the value of the payment-amount text input and disable/enable it
		var input = $(event.currentTarget);
		var disable = input.data('disable') || false;
		var amount = input.data('amount');
		$('#payment-amount').attr('value', amount);
		if (disable) {
			$('#payment-amount').prop('disabled', true).addClass('slvzr-disabled');
		} else {
			$('#payment-amount').prop('disabled', false).removeClass('slvzr-disabled');
		}
	});

	$('.payment-details-card-registered-address').find('input[type=radio]').change(function(event) {
		// When the user clicks the radio button indicating that payment address is not the same
		// Then we need to show the address fields
		var input = $(event.currentTarget);
		var show = input.data('show-address') || false;
		if (show) {
			$('.billing-address-container').slideDown();
		} else {
			$('.billing-address-container').slideUp();
		}
	});
	// END PAYMENT RADIO LOGIC
	
	// Function to get Credit card type from Credit card number and show correct images
	function GetCardType(number){
		$('.credit-card-types .credit-card-front').removeClass('selected');
		$('.credit-card-back').hide();
		var cardType = null;
		var re = new RegExp("^4");
		if (number.match(re) !== null)
			cardType = "visa";
		re = new RegExp("^(34|37)");
		if (number.match(re) !== null)
			cardType = "amex";
		re = new RegExp("^5[1-5]");
		if (number.match(re) !== null)
			cardType = "master";
		re = new RegExp("^6011");
		if (number.match(re) !== null)
			cardType = "discover";
		console.log(cardType);
		if (cardType !== null){
			var frontTarget = '.credit-card-' + cardType;
			$(frontTarget).addClass('selected');
			$('.card-back-' + cardType).fadeIn();
		}
	}

	$("#card-number").change(function() {
		var currentInput = $('#card-number').val();
		console.log(currentInput);
		GetCardType(currentInput);
	});

	// BEGIN CREDIT CARD DISPLAY INTERACTION
	$('.credit-card-types .credit-card-front').click(function(event) {
		var target = $(event.currentTarget);
		var cardType = target.attr('class').split(' ')[0].split('-')[2];
		$('.credit-card-types .credit-card-front').removeClass('selected');
		target.addClass('selected');
		$('.credit-card-back').hide();
		$('.card-back-' + cardType).fadeIn();
	});
	// END CREDIT CARD DISPLAY INTERACTION

	// BEGIN TERMS ENABLE AND DISABLE SUBMIT
	var input = $('#terms-checkbox');
	if (input.is(':checked'))
		$('#submit-button').removeClass('button-disabled');
	else
		$('#submit-button').addClass('button-disabled');
	input.change(function(event) {
		if (input.is(':checked'))
			$('#submit-button').removeClass('button-disabled');
		else
			$('#submit-button').addClass('button-disabled');		
	});
	// END TERMS ENABLE AND DISABLE SUBMIT
});

// 10.0 - BOOKING CONFIRMATION
$(function() {
	// BEGIN SHARE BUTTONS
	$('.twitter-button, .facebook-button, .pinterest-button').click(function(event) {
		target = $(event.currentTarget);
		height = target.data('height') || 500;
		width = target.data('width') || 300;
		event.preventDefault();
		window.open(target.attr('href'), '_blank','width=' + width + ',height=' + height + ',top=300,left=300');
	});
	// END SHARE BUTTONS
});



// GENERAL SITE WIDE JS
$(function(){
	// BEGIN POPUP EXPLANATION 
	// This is for elements which when clicked show a little help element under the link
	// Listen for clicks on elements like <a href="#" class="popup-link" data-popup="prepaid-gratuities">...</a>
	// Then find the corresponding popup element, position it and unhide it
	$('.popup-link').click(function(event) {
		// We need to stop propagation as otherwise the listener on html will get triggered
		event.stopPropagation();

		event.preventDefault();

		ensurePopupsCreated();

		target = $(event.currentTarget);
		targetWidth = target.width();

		// find the popup element to show based on the data-popup attribute on the link element
		var popupElement = $('#popup-' + target.data('popup'));
		// get the absolute offset of the link, remove the popup element from DOM
		// then reattach as a child of the booking-popups element.
		// we need to do this so we can accurately position it without relying on interference from 
		// parents with position: relative;
		offset = target.offset();
		popupElement.remove();
		popupsContainer.append(popupElement);
		if (popupElement.hasClass('popup-explanation-left')) {
			popupElement.removeClass('hide').css({
				position: 'absolute',
				top: (offset.top - 24),
				left: (offset.left - 50 - popupElement.width())
			});
		} else if (popupElement.hasClass('popup-explanation-up-aligned-right')) {
			popupElement.removeClass('hide').css({
				position: 'absolute',
				top: (offset.top + (target.height())),
				left: (offset.left + 25 + (targetWidth / 2) - (popupElement.width()))
			});
		} else {
			popupElement.removeClass('hide').css({
				position: 'absolute',
				top: (offset.top + (target.height())),
				left: (offset.left - 92 + (targetWidth / 2))
			});
		}
		popupElement.click(function(event) {
			event.stopPropagation();
		});
		popupElement.find('.icon-remove-sign').click(function(event) {
			event.preventDefault();
			$('.popup-explanation').addClass('hide');
		});
	});
	$(window).load(function() {
		$('.popup-link-show-on-load').click();
	});
	$.subscribe('expander:expand', function(event, id) {
		$('.popup-explanation').addClass('hide');
	});
	$(window).resize(function(event) {
		$('.popup-explanation').addClass('hide');
	});
	$('html').click(function() {
		$('.popup-explanation').addClass('hide');
	});
	// END POPUP EXPLANATION

	// BEGIN EXPANDER
	// This is for links which when clicked slide down other sections
	$('.expander').click(function(event) {
		// We need to stop propagation as otherwise the listener on html will get triggered
		event.stopPropagation();
		event.preventDefault();

		// identify the link being clicked
		var target = $(event.currentTarget);
		
		// get the target expandable as defined in the link data attribute
		var expandableElement = $('#' + target.data('target'));
		
		// open or close the expandable
		expandableElement.slideToggle('slow', function() {
			// switch the link state
			if ($(this).is(':hidden')) {
				target.find('.expander-open-link').hide();
				target.find('.expander-closed-link').show();
			} else {
				target.find('.expander-open-link').show();
				target.find('.expander-closed-link').hide();
			}
		});

		// publish this event
		$.publish('expander:expand', target.data('target'));
	});
	// END EXPANDER
});


// GENERAL SITE WIDE JS
$(function(){
	$.fn.clearValidationErrors = function(params) {
		var form = this;
		form.find('.validation-summary').hide().find('.content ul li').remove();
		form.find('.error-block').remove();
	};

	$.fn.showValidationError = function(params) {
		var form = this;

		var fieldName = params.fieldName;
		var errorMessage = params.errorMessage;

		// add error message to validation summary
		var errorNode = $('<li></li>').text(errorMessage);
		form.find('.validation-summary').show().find('.content ul').append(errorNode);

		// create error block to place above form field from a template present in the HTML
		var errorBlock = $('#error-block-template').clone();
		var fieldElement = form.find('[name=' + fieldName + ']');
		errorBlock.removeClass('hide').attr('id', '').find('.error-block-message').text(errorMessage);
		fieldElement.parent().prepend(errorBlock);
	};

	// CODEGEN NOTE: EXAMPLE USAGE, DELETE THIS
	$('form').clearValidationErrors();
	$('form').showValidationError({
		fieldName: 'guest1-email-address',
		errorMessage: 'Please enter valid email address'
	});
	window.setTimeout(function() {
		// when handling form validation, clear the form with clearValidationErrors and
		// call showValidationError once for each error field
		$('form').clearValidationErrors();
		$('form').showValidationError({
			fieldName: 'guest1-phone-number',
			errorMessage: 'Please enter valid phone number'
		});
		$('form').showValidationError({
			fieldName: 'guest1-date-of-birth',
			errorMessage: 'Please enter valid date of birth'
		});
	}, 3000);
	// END CODEGEN NOTE: EXAMPLE USAGE
});


// GENERIC LIGHTBOX JS
(function( $ ){
	$.fn.bookingLightbox = function(params) {
		// create container element for our lightbox which we will hang events off
		var element = this;

		// no clone is used if for some reason cloning the element causes problems with other JS, 
		// such as with select boxes in Celebrity skin
		if (!params.noClone) {
			element = this.clone(true);
		}

		if (!params.fixed) {
			element.height($('body').css('height'));
		}

		if (!element.data('lightbox-init')) {
			// set up the close event and listeners which will destroy the lightbox
			element.bind('close', function() {
				$(this).fadeOut(function() {
					if (!params.noClone)
						$(this).remove();
				});
			});
			element.find('.link-lightbox-close, .lightbox-remove, .lightbox-close, .icon-remove-sign').click(function(event) {
				event.preventDefault();
				element.trigger('close');
			});
			element.find('.lightbox-main-container').click(function(event) {
				event.stopPropagation();
			});
			element.click(function() {
				element.trigger('close');
			});

			// create the container for the lightbox
			ensurePopupsCreated();

			// append the lightbox to the container
			$('.booking-popups').append(element);
			element.data('lightbox-init', true);
		}

		if (!params.noShow) {
			element.fadeIn();
		}

		return element;
	};
})( jQuery );


$(function() {
	$('.show-lightbox').click(function(event) {
		event.stopPropagation();
		event.preventDefault();

		var target = $(event.currentTarget);
		$('#' + target.data('lightbox')).bookingLightbox({
			fixed: target.data('fixed') || false,
			noClone: target.data('no-clone') || false
		});
	});
});