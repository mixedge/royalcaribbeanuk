<%@ Page Language="C#" %>

<script runat="server">
    protected string FromStop { get; set; }
    protected string ToStop { get; set; }
    
    protected void Page_Load(object sender, EventArgs e)
    {
        FromStop = "000000";
        if (!string.IsNullOrEmpty(Request.QueryString["from"]))
            FromStop = Request.QueryString["from"];

        ToStop = "000000";
        if (!string.IsNullOrEmpty(Request.QueryString["to"]))
            ToStop = Request.QueryString["to"];
    }
</script>
<?xml version="1.0"?>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="100%" height="100%">
    <defs>
        <linearGradient id="linear-gradient" x1="0%" y1="0%" x2="0%" y2="100%">
            <stop offset="0%" stop-color="#<%=FromStop%>" stop-opacity="1"/>
            <stop offset="100%" stop-color="#<%=ToStop%>" stop-opacity="1"/>
        </linearGradient>
    </defs>
    <rect width="100%" height="100%" fill="url(#linear-gradient)"/>
</svg>