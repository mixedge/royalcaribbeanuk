/* Moment.js | version : 1.4.0 | author : Tim Wood | license : MIT */
(function(a,b){function r(a){this._d=a}function s(a,b){var c=a+"";while(c.length<b)c="0"+c;return c}function t(b,c,d,e){var f=typeof c=="string",g=f?{}:c,h,i,j,k;return f&&e&&(g[c]=+e),h=(g.ms||g.milliseconds||0)+(g.s||g.seconds||0)*1e3+(g.m||g.minutes||0)*6e4+(g.h||g.hours||0)*36e5,i=(g.d||g.days||0)+(g.w||g.weeks||0)*7,j=(g.M||g.months||0)+(g.y||g.years||0)*12,h&&b.setTime(+b+h*d),i&&b.setDate(b.getDate()+i*d),j&&(k=b.getDate(),b.setDate(1),b.setMonth(b.getMonth()+j*d),b.setDate(Math.min((new a(b.getFullYear(),b.getMonth()+1,0)).getDate(),k))),b}function u(a){return Object.prototype.toString.call(a)==="[object Array]"}function v(b){return new a(b[0],b[1]||0,b[2]||1,b[3]||0,b[4]||0,b[5]||0,b[6]||0)}function w(b,d){function u(d){var e,j;switch(d){case"M":return f+1;case"Mo":return f+1+q(f+1);case"MM":return s(f+1,2);case"MMM":return c.monthsShort[f];case"MMMM":return c.months[f];case"D":return g;case"Do":return g+q(g);case"DD":return s(g,2);case"DDD":return e=new a(h,f,g),j=new a(h,0,1),~~((e-j)/864e5+1.5);case"DDDo":return e=u("DDD"),e+q(e);case"DDDD":return s(u("DDD"),3);case"d":return i;case"do":return i+q(i);case"ddd":return c.weekdaysShort[i];case"dddd":return c.weekdays[i];case"w":return e=new a(h,f,g-i+5),j=new a(e.getFullYear(),0,4),~~((e-j)/864e5/7+1.5);case"wo":return e=u("w"),e+q(e);case"ww":return s(u("w"),2);case"YY":return s(h%100,2);case"YYYY":return h;case"a":return m>11?t.pm:t.am;case"A":return m>11?t.PM:t.AM;case"H":return m;case"HH":return s(m,2);case"h":return m%12||12;case"hh":return s(m%12||12,2);case"m":return n;case"mm":return s(n,2);case"s":return o;case"ss":return s(o,2);case"zz":case"z":return(b.toString().match(l)||[""])[0].replace(k,"");case"Z":return(p>0?"+":"-")+s(~~(Math.abs(p)/60),2)+":"+s(~~(Math.abs(p)%60),2);case"ZZ":return(p>0?"+":"-")+s(~~(10*Math.abs(p)/6),4);case"L":case"LL":case"LLL":case"LLLL":case"LT":return w(b,c.longDateFormat[d]);default:return d.replace(/(^\[)|(\\)|\]$/g,"")}}var e=new r(b),f=e.month(),g=e.date(),h=e.year(),i=e.day(),m=e.hours(),n=e.minutes(),o=e.seconds(),p=-e.zone(),q=c.ordinal,t=c.meridiem;return d.replace(j,u)}function x(b,d){function p(a,b){var d;switch(a){case"M":case"MM":e[1]=~~b-1;break;case"MMM":case"MMMM":for(d=0;d<12;d++)if(c.monthsParse[d].test(b)){e[1]=d;break}break;case"D":case"DD":case"DDD":case"DDDD":e[2]=~~b;break;case"YY":b=~~b,e[0]=b+(b>70?1900:2e3);break;case"YYYY":e[0]=~~Math.abs(b);break;case"a":case"A":l=b.toLowerCase()==="pm";break;case"H":case"HH":case"h":case"hh":e[3]=~~b;break;case"m":case"mm":e[4]=~~b;break;case"s":case"ss":e[5]=~~b;break;case"Z":case"ZZ":h=!0,d=(b||"").match(o),d&&d[1]&&(f=~~d[1]),d&&d[2]&&(g=~~d[2]),d&&d[0]==="+"&&(f=-f,g=-g)}}var e=[0,0,1,0,0,0,0],f=0,g=0,h=!1,i=b.match(n),j=d.match(m),k,l;for(k=0;k<j.length;k++)p(j[k],i[k]);return l&&e[3]<12&&(e[3]+=12),l===!1&&e[3]===12&&(e[3]=0),e[3]+=f,e[4]+=g,h?new a(a.UTC.apply({},e)):v(e)}function y(a,b){var c=Math.min(a.length,b.length),d=Math.abs(a.length-b.length),e=0,f;for(f=0;f<c;f++)~~a[f]!==~~b[f]&&e++;return e+d}function z(a,b){var c,d=a.match(n),e=[],f=99,g,h,i;for(g=0;g<b.length;g++)h=x(a,b[g]),i=y(d,w(h,b[g]).match(n)),i<f&&(f=i,c=h);return c}function A(a,b,d){var e=c.relativeTime[a];return typeof e=="function"?e(b||1,!!d,a):e.replace(/%d/i,b||1)}function B(a,b){var c=d(Math.abs(a)/1e3),e=d(c/60),f=d(e/60),g=d(f/24),h=d(g/365),i=c<45&&["s",c]||e===1&&["m"]||e<45&&["mm",e]||f===1&&["h"]||f<22&&["hh",f]||g===1&&["d"]||g<=25&&["dd",g]||g<=45&&["M"]||g<345&&["MM",d(g/30)]||h===1&&["y"]||["yy",h];return i[2]=b,A.apply({},i)}function C(a,b){c.fn[a]=function(a){return a!=null?(this._d["set"+b](a),this):this._d["get"+b]()}}var c,d=Math.round,e={},f=typeof module!="undefined",g="months|monthsShort|monthsParse|weekdays|weekdaysShort|longDateFormat|calendar|relativeTime|ordinal|meridiem".split("|"),h,i=/^\/?Date\((\d+)/i,j=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|dddd?|do?|w[o|w]?|YYYY|YY|a|A|hh?|HH?|mm?|ss?|zz?|ZZ?|LT|LL?L?L?)/g,k=/[^A-Z]/g,l=/\([A-Za-z ]+\)|:[0-9]{2} [A-Z]{3} /g,m=/(\\)?(MM?M?M?|dd?d?d|DD?D?D?|YYYY|YY|a|A|hh?|HH?|mm?|ss?|ZZ?|T)/g,n=/(\\)?([0-9]+|([a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+|([\+\-]\d\d:?\d\d))/gi,o=/([\+\-]|\d\d)/gi,p="1.4.0",q="Month|Date|Hours|Minutes|Seconds|Milliseconds".split("|");c=function(c,d){if(c===null)return null;var e,f;return c&&c._d instanceof a?e=new a(+c._d):d?u(d)?e=z(c,d):e=x(c,d):(f=i.exec(c),e=c===b?new a:f?new a(+f[1]):c instanceof a?c:u(c)?v(c):new a(c)),new r(e)},c.version=p,c.lang=function(a,b){var d,h,i,j=[];if(b){for(d=0;d<12;d++)j[d]=new RegExp("^"+b.months[d]+"|^"+b.monthsShort[d].replace(".",""),"i");b.monthsParse=b.monthsParse||j,e[a]=b}if(e[a])for(d=0;d<g.length;d++)h=g[d],c[h]=e[a][h]||c[h];else f&&(i=require("./lang/"+a),c.lang(a,i))},c.lang("en",{months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),longDateFormat:{LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D YYYY",LLL:"MMMM D YYYY LT",LLLL:"dddd, MMMM D YYYY LT"},meridiem:{AM:"AM",am:"am",PM:"PM",pm:"pm"},calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[last] dddd [at] LT",sameElse:"L"},relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},ordinal:function(a){var b=a%10;return~~(a%100/10)===1?"th":b===1?"st":b===2?"nd":b===3?"rd":"th"}}),c.fn=r.prototype={clone:function(){return c(this)},valueOf:function(){return+this._d},"native":function(){return this._d},toString:function(){return this._d.toString()},toDate:function(){return this._d},format:function(a){return w(this._d,a)},add:function(a,b){return this._d=t(this._d,a,1,b),this},subtract:function(a,b){return this._d=t(this._d,a,-1,b),this},diff:function(a,b,e){var f=c(a),g=(this.zone()-f.zone())*6e4,h=this._d-f._d-g,i=this.year()-f.year(),j=this.month()-f.month(),k=this.date()-f.date(),l;return b==="months"?l=i*12+j+k/30:b==="years"?l=i+j/12:l=b==="seconds"?h/1e3:b==="minutes"?h/6e4:b==="hours"?h/36e5:b==="days"?h/864e5:b==="weeks"?h/6048e5:h,e?l:d(l)},from:function(a,b){var d=this.diff(a),e=c.relativeTime,f=B(d,b);return b?f:(d<=0?e.past:e.future).replace(/%s/i,f)},fromNow:function(a){return this.from(c(),a)},calendar:function(){var a=this.diff(c().sod(),"days",!0),b=c.calendar,d=b.sameElse,e=a<-6?d:a<-1?b.lastWeek:a<0?b.lastDay:a<1?b.sameDay:a<2?b.nextDay:a<7?b.nextWeek:d;return this.format(typeof e=="function"?e.apply(this):e)},isLeapYear:function(){var a=this.year();return a%4===0&&a%100!==0||a%400===0},isDST:function(){return this.zone()<c([this.year()]).zone()||this.zone()<c([this.year(),5]).zone()},day:function(a){var b=this._d.getDay();return a==null?b:this.add({d:a-b})},sod:function(){return this.clone().hours(0).minutes(0).seconds(0).milliseconds(0)},eod:function(){return this.sod().add({d:1,ms:-1})}};for(h=0;h<q.length;h++)C(q[h].toLowerCase(),q[h]);C("year","FullYear"),c.fn.zone=function(){return this._d.getTimezoneOffset()},f&&(module.exports=c),typeof window!="undefined"&&(window.moment=c),typeof define=="function"&&define.amd&&define("moment",[],function(){return c})})(Date)

/*Pack in Much More*/
var ltIE9 = $.browser.msie && parseFloat($.browser.version) < 9;

var autoplayPIMMLink, autoplayPIMMSlideShow; //timers for auto play link and slide show
$(document).ready(function(){
	// check for countdown
	if ($('#pimmOverlayInner').hasClass('countdown')) {
		var target = $('#pimmOverlayInner').attr('data-targetdate');
		
		var timeTag = $("<time datetime='" + target + "'></time>");
		if (ltIE9) {
			timeTag = document.createElement('time');
			timeTag['datetime'] = target;
		}
		
		var tabOneHeading = $('#pimmTab1 .pimmColRight h2');
		if (tabOneHeading.length > 0) {
			tabOneHeading.after(timeTag);
		} else {
			$('#pimmTab1 .pimmColRight').prepend(timeTag);
		}
		
		$('#pimmOverlayInner time').append("<span></span>");
	}

	//animate case
	var docWidth = $(document).width(); //get document width for case animation
	$('#pimmBarInnerCase').css('left', 0 - (docWidth / 2)); //position the case off to the left by half screen width
	$('#pimmBarInnerCase').show().delay(250).animate({'left': 175}, 1500); //animate case back to original position
	
	//assign click functions
	$('#pimmBarInnerLink').click(function(){ showPIMM() }); //add click to blue bar link
	$('#pimmBarInnerCase').click(function(){ showPIMM() }); //add click to case
	$('#pimmTabLabels li').click(function(){ //add click to each tab label
		$('#pimmTabLabels li').removeClass('selected'); //remove any selected class from all labels
		showPIMMTab($(this).attr('class')); //show the tab id that matches the class name
		$(this).addClass('selected'); //set this tab label to selected
	});
	
	//initiate slide show
	$('.pimmSlideShow').each(function() { //for each slide show
		var slideshowObject = $(this); //get the slide show object
		var slideshowWidth = parseInt(slideshowObject.css('width').replace('px',''), 10); //get the width of the slide show
		var innerWidth = 0; //default the inner width of the slide show
		slideshowObject.append('<div class="pimmSlideShowPage"></div>'); //add slide page holder
		slideshowObject.parent().append('<div class="pimmButton pimmButtonLeft"></div>'); //add left button
		slideshowObject.parent().append('<div class="pimmButton pimmButtonRight"></div>'); //add right button
		slideshowObject.find('li').each(function() { //for each slide within the slide show
			innerWidth = innerWidth + parseInt($(this).css('width').replace('px',''), 10); //add the width of this slide to the total of the slide show
			slideshowObject.find('.pimmSlideShowPage').append('<span></span>'); //add a page for this slide
		});
		slideshowObject.find('ul').css('width',innerWidth); //set the width of the slide show to the value we just collected from individual slide widths

		//hide unneeded controls if only one slide exists
		var numberOfPages = innerWidth/slideshowWidth; //calculate the number of slides in this slide show
		if(numberOfPages < 2){ // if there are fewer than 2 slides
			slideshowObject.find('.pimmSlideShowPage').css('display', 'none'); //hide the slide pages
			slideshowObject.parent().find('.pimmButton').css('display', 'none'); //hide the slide controls
		} else {
			slideshowObject.find('.pimmSlideShowPage').css('left', ((slideshowWidth/2)-20) - ((numberOfPages*20)/2)); //centre the pages
		}
	});
	playPIMMLink(); //start the link animation
	assignLRPIMM(); //assign slide show controls we've just created
	
	//insert rounded corners for <IE9
	$('.pimmTab').append('<div class="ieTopLeft"></div><div class="ieTopRight"></div>'); //add divs to the tabs
	$('#pimmTabLabels li').append('<div class="ieTopLeft"></div><div class="ieTopRight"></div>'); //add dives to the tab labels
});
function showPIMM() { //show the overlay
	//remove click functions
	$('#pimmBarInnerCase').unbind('click'); //remove click to show from the case
	$('#pimmBarInnerLink').unbind('click'); //remove click to show from the link
	$('#pimmBarInnerLink').removeClass(); //remove the current class from the link
	$('#pimmBarInnerLink').addClass('down'); //add the down class to the link
	clearTimeout(autoplayPIMMLink); //stop the link animation
	
	//hide case
	if (ltIE9) {
		$('#pimmBarInnerCase').hide();
	} else {
		$('#pimmBarInnerCase').animate({opacity: 0}, function() { //fade out the case
			$('#pimmBarInnerCase').css('display', 'none'); //hide the case once faded
		});
	}

	//fade out bg
	$('body').append('<div id="pimmOverlayBG"></div>'); //add the background tint
	$('#pimmOverlayBG').css('height', $(document).height()); //set height to that of the document
	$('#pimmOverlayBG').css('opacity', 0); //set opacity of the tint to 0 as we want to fade it in
	$('#pimmOverlayBG').animate({opacity: 0.8}, function(){ //fade in the tint
		//show overlay
		$('#pimmTabLabels li').removeClass('selected'); //deselect all of the tabs
		showPIMMTab('pimmTab1'); //reset tab 1 for the default view
		
		//get tallest tab height
		var tabHeight = 0; //default the tab height
		$('.pimmTab').each(function() { //for each tab in the overlay
			if(tabHeight<$(this).height()) //if the height is taller than our current value
				tabHeight = $(this).height(); //set the value to the tallest
		});
		tabHeight = tabHeight + 30; //add some margin
		$('.pimmTab').css('height',tabHeight); //set all tabs to the tallest value we found
		
		$('#pimmOverlay').animate({'height': tabHeight}, function(){ //animate the overlay to the tallest tab height
			//allow close
			$('#pimmOverlayBG').click(function(){ hidePIMM() }); //add click to close to the background tint
			$('#pimmBarInnerLink').click(function() { hidePIMM() }); //add click to close to the link
			$('#pimmClose').click(function(){ hidePIMM() }); //add click to close to the close button
		});
	});
}
function hidePIMM() { //hide the overlay
	//remove click functions
	$('#pimmOverlayBG').unbind('click'); //remove click to close from the background tint
	$('#pimmClose').unbind('click'); //remove click to close from the close button
	$('#pimmBarInnerLink').unbind('click'); //remove click to close from the link
	$('#pimmBarInnerLink').removeClass(); //remove the class from the link
	
	//show last item in link animation
	$('#pimmBarInnerLink li:not(.back)').removeClass(); //forget the link text animation
	$('#pimmBarInnerLink li:not(.back):last').addClass('show'); //set the link text to the last frame in the animation
	
	//hide overlay
	clearTimeout(autoplayPIMMSlideShow); //clear the slide show animation
	
	$('#pimmOverlay').animate({'height': 0}, function(){ //animate the overlay height back to 0
		// reset tab heights
		$('.pimmTab').removeAttr('style');
		
		//show case
		$('#pimmBarInnerCase').css('display', 'block'); //display the case
		
		if (ltIE9) {
			$('#pimmBarInnerCase').show().click(function() { showPIMM() }); //add click to show to the case
		} else {
			$('#pimmBarInnerCase').animate({opacity: 1}, function(){ //fade the case in
				$('#pimmBarInnerCase').click(function() { showPIMM() }); //add click to show to the case
			});
		}
		
		//fade in bg
		$('#pimmOverlayBG').animate({opacity: 0}, function(){ //fade out the background tint
			$('#pimmOverlayBG').remove(); //remove the background tint
			$('#pimmBarInnerLink').click(function() { showPIMM() }); //add click to show to the link
		});
	});
}
function showPIMMTab(id) { //reset and show tab
	id = '#' + id; //clean the tab id so it's ready to use

	var dateEl = $(id + ' time');
	if (dateEl.length > 0) {
		try {
			var targetDateString = dateEl.attr('datetime');
			var targetDate = moment(targetDateString, 'DD/MM/YYYY');
			
			var days = targetDate.diff(moment(), 'days', true);
			days = Math.ceil(days);

			var span = dateEl.find('span');
			span.text(days);

			if (days > 9) {
				span.css('left', '20px');
			}

			if (days > 0) {
				dateEl.css('display', 'block');
			}
		}
		catch (err) {}
	}
	
	//move selected tab to front
	$('.pimmTab').css('display', 'none'); //hide all of the tabs
	$(id).css('display', 'block'); //show the tab passed into the function
	
	//reset slide show on current tab if one exists
	clearTimeout(autoplayPIMMSlideShow); //clear the slide show animation
	if($(id + ' .pimmSlideShow').length > 0) { //if this tab has a slide show
		$(id + ' .pimmSlideShowPage span').removeClass('selected'); //deselect all pages
		$(id + ' .pimmSlideShowPage span').first().addClass('selected'); //select the first page
		$('.pimmSlideShow ul').css('left', 0); //show the first slide
		
		if ($(id + ' .pimmSlideShowPage').is(':visible')) {
			autoplayPIMMSlideShow = setTimeout(function(){playPIMMSlideShow(id, true, 'right')}, 3000); //start the slide show animation
		}
	}
}
function playPIMMLink() { //play the bar messages
	clearTimeout(autoplayPIMMLink); //stop the link animation
	
	//find the first or next link text to show
	if($('#pimmBarInnerLink li.show').length < 1) { //if no text is selected
		$('#pimmBarInnerLink li:not(.back):first').addClass('show'); //select the first text item
	} else {
		$('#pimmBarInnerLink li.show').addClass('shown'); //set showing text to shown
		$('#pimmBarInnerLink li.show').removeClass('show'); //remove the showing class
		$('#pimmBarInnerLink li:not(.back):not(.shown):first').addClass('show'); //show the next text item
	}

	//loop if there are more items to show
	if($('#pimmBarInnerLink li:not(.back):not(.show):not(.shown)').length > 0) //if there are any text items left to show
		autoplayPIMMLink = setTimeout(function(){playPIMMLink()}, 3000); //start the link animation
	else {
		setTimeout(function() {
			$('#pimmBarInnerLink li').removeClass('shown').removeClass('show');
			playPIMMLink();
		}, 3000);
	}
}
function playPIMMSlideShow(id, auto, direction) { //slide show functions - requires tab id, auto play and slide direction
	clearTimeout(autoplayPIMMSlideShow); //stop the slide show animation
	$('.pimmButton').unbind('click'); //remove the click from the controls
	
	var left = $(id + ' ul').css('left').replace('-','').replace('px',''); //find out how far we've slid so far
	var innerWidth = $(id + ' ul').width(); //get the total width of the slide show
	var slideWidth = $(id + ' .pimmSlideShow').width(); //get the width of the slides
	var numberOfSlides = innerWidth / slideWidth; //calculate the number of slides in the slide show
	var currSlide = (left / slideWidth) + 1; //calculate the current slide showing
	
	//slide to next slide
	if(direction == 'right') { //if we're going right
		if(left == innerWidth - slideWidth) { //if we're at the end
			$(id + ' ul').animate({left: 0}); //slide to the start
			currSlide = 1; //set current slide to 1
		} else {
			$(id + ' ul').animate({left: '-=' + $(id + ' .pimmSlideShow').width()}); //slide to the next slide
			currSlide = currSlide + 1; //increase the current slide number
		}
	} else {
		if(left == 0) { //if we're at the start
			$(id + ' ul').animate({left: '-' + (innerWidth - slideWidth)}); //slide to the end
			currSlide = numberOfSlides; //set current slide to last slide
		} else {
			$(id + ' ul').animate({left: '+=' + $(id + ' .pimmSlideShow').width()}); //slide to the previous slide
			currSlide = currSlide - 1; //decrease the current slide number
		}
	}
	
	//update pager
	$(id + ' .pimmSlideShowPage span').removeClass('selected'); //deselect the current page
	$(id + ' .pimmSlideShowPage span:nth-child(' + currSlide + ')').addClass('selected'); //select the new current page
	
	setTimeout(function(){assignLRPIMM()}, 500); //re-assign controls
	
	if(auto) //if we're automatically playing the slide show
		autoplayPIMMSlideShow = setTimeout(function(){playPIMMSlideShow(id, true, 'right')}, 3000); //start the slide show animation
}
function assignLRPIMM(){ //assign the slide show controls
	$('.pimmButton').each(function(){ //for each slide show control
		var buttonObject = $(this); //get the control object
		var direction = 'left'; //default direction to left
		if(buttonObject.hasClass('pimmButtonRight')) //if the class says the button is right direction
			direction = 'right'; //set direction to right
		buttonObject.click(function(){ //add click function
			playPIMMSlideShow('#' + buttonObject.parent().parent().attr('id'), false, direction); //call the slide function passing
		});
	});
}